package hello.domain;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class IDCard {

    // system generated
    private String card_id;
  
    // required
    @NotNull
    private String card_name;
     @NotNull
    private String card_number;

    private String expiration_date;
    
    public IDCard(){
        this.card_id = ""; 
        this.card_name = "";
        this.card_number = "";
        this.expiration_date = "";
    }
    
    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getCard_id() {
        return this.card_id;
    }
    
    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_name() {
        return this.card_name;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_number() {
        return this.card_number;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getExpiration_date() {
        return this.expiration_date;
    } 
    @Override
    public String toString() {
        return String.format(
                "IDCard [card_id=%s, card_name='%s', card_number='%s', expiration_date='%s']",
                card_id, card_name, card_number, expiration_date);
    }
}   
