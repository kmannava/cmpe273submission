package hello.domain;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;
import java.util.ArrayList;

@Document
public class User {

    // system generated
    public String user_id;
    private Date updated_at;
    private Date created_at;
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    // required
    @NotNull
    //TODO @Pattern(regexp="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$")
    public String email;
    @NotNull
    public String password;

    //optional
    public String name;
   

    ArrayList <WebLogin> weblogins;
    ArrayList <IDCard> idcards;
    ArrayList <BankAccount> accounts;

    public User(){
        this.user_id = "";
        this.email = "";
        this.password = "";
        this.name = "";
        this.updated_at = null;
        this.created_at = null;
        this.weblogins = new ArrayList<WebLogin>();
        this.idcards = new ArrayList<IDCard>();
        this.accounts = new ArrayList<BankAccount>();
    }
    
    public void setCreated_at(Date time) {
        this.created_at = time;
    }

    public String getCreated_at() {
        return sdf.format(this.created_at);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    
    public void setPassword(String pwd) {
        this.password = pwd;
    }

    public String getPassword() {
        return this.password;
    }
 
    public void setEmail(String email) {
        this.email = email;
    } 
    public String getEmail() {
        return this.email;
    }


    public void setUpdated_at(Date time) {
        this.updated_at = time;
    }

    public String getUpdated_at() {
        return sdf.format(this.updated_at);
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }
    // weblogin
    public void addWeblogin(WebLogin web) {
        this.weblogins.add(web);
    }

    public void setWeblogins(ArrayList<WebLogin> weblogins) {
        this.weblogins = weblogins;
    }
    public ArrayList<WebLogin> myWlogins() {
        return this.weblogins;
    }
    public boolean removeWL(String login_id) {
        for (WebLogin wl: this.weblogins) {
            if(wl.getLogin_id().equals(login_id)) {
                this.weblogins.remove(wl);
                return true;
            }
        }
        return false;
    }
    // idcard
    public void addIDCard(IDCard card) {
        this.idcards.add(card);
    }

    public void setIDCards(ArrayList<IDCard> weblogins) {
        this.idcards = weblogins;
    }
    public ArrayList<IDCard> myIDCards() {
        return this.idcards;
    }
    public boolean removeIDCard(String card_id) {
        for (IDCard c: this.idcards) {
            if(c.getCard_id().equals(card_id)) {
                this.idcards.remove(c);
                return true;
            }
        }
        return false;
    }

    // idcard
    public void addBankAccount(BankAccount acc) {
        this.accounts.add(acc);
    }

    public void setBankAccounts(ArrayList<BankAccount> accs) {
        this.accounts = accs;
    }
    public ArrayList<BankAccount> myBankAccounts() {
        return this.accounts;
    }
    public boolean removeBankAccount(String ba_id) {
        for (BankAccount c: this.accounts) {
            if(c.getBa_id().equals(ba_id)) {
                this.accounts.remove(c);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%s, email='%s', password='%s', weblogins='%s', idcards=%s, accounts=%s]",
                user_id, email, password, weblogins, idcards, accounts);
    }
}
