package hello.repository;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import hello.domain.User;
import hello.domain.WebLogin; 
import hello.domain.IDCard; 
import hello.domain.BankAccount; 
import hello.exceptions.ResourceNotFoundException;
import hello.exceptions.InternalErrorException;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.lang.System.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import com.mongodb.MongoClient;
import org.springframework.web.client.RestTemplate;

import static org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
import java.security.cert.X509Certificate;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.springframework.web.client.RestTemplate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.*;
import java.security.cert.CertificateException;
import org.json.*;
@Repository
@Component

public class UserRepository {
    private final AtomicLong counter = new AtomicLong();

    //private MongoHelper mongohelper = MongoHelper.getInstance();
    private MongoOperations mongoOperations = null;

    private void init() {
        try {
            mongoOperations = new MongoTemplate(new MongoClient("ds049160.mongolab.com", 49160),
                                                            "cmpe273db",
                                                            new UserCredentials("test1", "test1234"));
            if (mongoOperations.collectionExists(User.class)) {
                mongoOperations.dropCollection(User.class);
            }

            mongoOperations.createCollection(User.class);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    // mongodb helper classes
    private User mongoHelperFindUser(String userid) {
        if (mongoOperations == null) {
            init();
        }
        Criteria criteria = Criteria.where("user_id").is(userid);
        List <User> users = mongoOperations.find(Query.query(criteria), User.class);
        if (!users.isEmpty()) { 
            return users.get(0);
        } 
        return null; 
    }
    private User mongoHelperUpdateUser(String user_id, final User newValue) {
        if (mongoOperations == null) {
            init();
        }
        Criteria criteria = Criteria.where("user_id").is(user_id);
        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("name", newValue.name),
                                    User.class);
        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("password", newValue.password),
                                    User.class);

        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("updated_at", getCurrentTimestamp()),
                                    User.class);
        return mongoHelperFindUser(user_id);
    }

    private User mongoHelperUpdateUserWLogins(String user_id, ArrayList<WebLogin> ll) {
        if (mongoOperations == null) {
            init();
        }
        Criteria criteria = Criteria.where("user_id").is(user_id);
        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("weblogins", ll),
                                    User.class);
        return mongoHelperFindUser(user_id);
    }
    private User mongoHelperUpdateUserIDCards(String user_id, ArrayList<IDCard> ll) {
        if (mongoOperations == null) {
            init();
        }
        Criteria criteria = Criteria.where("user_id").is(user_id);
        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("idcards", ll),
                                    User.class);
        return mongoHelperFindUser(user_id);
    }
    private User mongoHelperUpdateUserAccounts(String user_id, ArrayList<BankAccount> ll) {
        if (mongoOperations == null) {
            init();
        }
        Criteria criteria = Criteria.where("user_id").is(user_id);
        mongoOperations.updateFirst(Query.query(criteria), 
                                    Update.update("accounts", ll),
                                    User.class);
        return mongoHelperFindUser(user_id);
    }
    
    public static void trustSelfSignedSSL(RestTemplate restTemplate) {
        System.out.println("hello");
        try {
            //System.setProperty("https.protocols", "SSL");
            SSLContext ctx = SSLContext.getInstance("SSLv3");
            X509TrustManager tm = new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            ctx.init(null, new TrustManager[]{tm}, new java.security.SecureRandom());
            //ctx.getSocketFactory().setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SSLContext.setDefault(ctx);
            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String validateRoutingNumber(String number) {
        RestTemplate restTemplate = new RestTemplate();
        //trustSelfSignedSSL(restTemplate);
        String url = "http://www.routingnumbers.info/api/data.json?rn=" + number;
        String result = restTemplate.getForObject(url, String.class, "Android");
        JSONObject obj = new JSONObject(result);
        int returnCode = obj.getInt("code");
        if (returnCode >= 300) {
            String mesg = "Routing number " + number + " is not valid.";
            throw new ResourceNotFoundException(number, mesg);
        }
        //System.out.println(returnCode);
        return obj.getString("customer_name");
    }

    //operations on User
    public User addUser(User newUser) {
        if (mongoOperations == null) {
            init();
        }
        if (newUser == null) {
            return null;
        }
        String newUserId = getNewUserId();
        newUser.setUser_id(newUserId);
        newUser.setCreated_at(getCurrentTimestamp());
        newUser.setUpdated_at(getCurrentTimestamp());
        mongoOperations.insert(newUser);
        return newUser;
    }

    public User getUser(String user_id) {
        if (user_id == null) {
            return null;
        }
        return mongoHelperFindUser(user_id);
    }
 
    public User modifyUser(String user_id, final User newValue) {
        return mongoHelperUpdateUser(user_id, newValue);
    }
    
    //operations for Weblogin

    public WebLogin addWebLogin(String user_id, 
                                WebLogin newWebLogin){
        User user = mongoHelperFindUser(user_id); 
        if (user != null) {
            setproperties(newWebLogin);
            user.addWeblogin(newWebLogin);
            mongoHelperUpdateUserWLogins(user_id, user.myWlogins());
            return newWebLogin;
        }
        String mesg = "User with id " + user_id + " not found";
        throw new ResourceNotFoundException(user_id, mesg);
    }

    public void setproperties(WebLogin newWebLogin){
        String login_id = "l-"+counter.incrementAndGet();
        newWebLogin.setLogin_id(login_id);
    }
    public WebLogin getWebLogin(String user_id, 
                                String login_id) {
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        } else {
            List <WebLogin> ll = user.myWlogins();
            for( WebLogin wl: ll) {
                if (wl.getLogin_id().equals(login_id)) {
                    return wl;
                }
            } 
        }
        String mesg = "Web login with id " + login_id + " not found";
        throw new ResourceNotFoundException(login_id, mesg);
    }

    public void deleteWebLogin(String user_id, 
                                String login_id) {
        User user = mongoHelperFindUser(user_id);
        boolean removed = false;
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        } else {
            if (user.removeWL(login_id)) {
                mongoHelperUpdateUserWLogins(user_id, user.myWlogins());
                removed = true;
            }
        }
        if (!removed) {
            String mesg = "Web login with id " + login_id + " not found";
            throw new ResourceNotFoundException(login_id, mesg);
        }
    }

    public ArrayList<WebLogin> getWebLogins(String user_id){
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String msg = String.format("User with id %s not found", user_id);
            throw new ResourceNotFoundException(user_id, msg);
        }
        return user.myWlogins();
    }


    private Date getCurrentTimestamp() {
        Date date= new Date();
        date.getTime();
        return date;
        //Timestamp ss = new Timestamp(date.getTime());
        //return ss.toString();
    }

    private String getNewUserId() {
        return "u-"+counter.incrementAndGet();
    }

    // operations IDCard
    private void checkUserIDPresent(String user_id) {
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
    }

    // Operations for IDCard
    public void setCardID(IDCard idcard){
        String card_id = "c-"+counter.incrementAndGet();
        idcard.setCard_id(card_id);
    }

    private String noUserCardMesg(String user_id, 
                                  String card_id) {
        String mesg = String.format("User %s has no card with id %s", 
                                    user_id, 
                                    card_id);
        return mesg;
    }

    public IDCard addIDCard(String user_id,
                            IDCard newIDCard){
        if (newIDCard == null) {
            throw new InternalErrorException("Internal Error: user_id shouldnt be null");
        }
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        setCardID(newIDCard);
        user.addIDCard(newIDCard);
        mongoHelperUpdateUserIDCards(user_id, user.myIDCards());
        return newIDCard;
    }

    public ArrayList<IDCard> getIDCards(String user_id){
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        return user.myIDCards();
    }

    public IDCard getIDCard(String user_id, 
                            String card_id) {
        if (user_id == null ||
            card_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or user_id shouldnt be null");
                                             
        }
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        ArrayList <IDCard> ll = user.myIDCards();
        for (IDCard card: ll) {
            if (card.getCard_id().equals(card_id)) {
                return card;
            }
        }
        throw new ResourceNotFoundException(card_id, 
                                            noUserCardMesg(user_id, card_id));
    }

    public void deleteIDCard(String user_id, 
                             String card_id) {
        if (user_id == null ||
            card_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or user_id shouldnt be null");
        }
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        boolean removed = false;
        if (user.removeIDCard(card_id)) {
            mongoHelperUpdateUserIDCards(user_id, user.myIDCards());
            removed = true;
        }
        if (!removed) { 
            throw new ResourceNotFoundException(card_id, 
                                                noUserCardMesg(user_id, card_id));
        }
    }

    // Operations for BankAccount
    public void setBaID(BankAccount ba){
        String ba_id = "b-"+counter.incrementAndGet();
        ba.setBa_id(ba_id);
    }

    private String noUserBAMesg(String user_id, 
                                  String ba_id) {
        String mesg = String.format("User %s has no bank account with id %s", 
                                    user_id, 
                                    ba_id);
        return mesg;
    }

    public BankAccount addBankAccount(String user_id,
                                      BankAccount newBankAccount){
        if (user_id == null) {
            throw new InternalErrorException("Internal Error: user_id shouldnt be null");
        }
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        String acc_name = validateRoutingNumber(newBankAccount.getRouting_number());
        newBankAccount.setAccount_name(acc_name);

        setBaID(newBankAccount);
        user.addBankAccount(newBankAccount);
        mongoHelperUpdateUserAccounts(user_id, user.myBankAccounts());
        return newBankAccount;
    }

    public ArrayList<BankAccount> getBankAccounts(String user_id){
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        return user.myBankAccounts();
    }

    public BankAccount getBankAccount(String user_id, 
                            String ba_id) {
        if (user_id == null ||
            ba_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or ba_id shouldnt be null");
        }
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        for (BankAccount card: user.myBankAccounts()) {
            if (card.getBa_id().equals(ba_id)) {
                return card;
            }
        }
        throw new ResourceNotFoundException(ba_id, 
                                            noUserBAMesg(user_id, ba_id));
    }

    public void deleteBankAccount(String user_id, 
                                  String ba_id) {
        if (user_id == null ||
            ba_id == null) {
            throw new InternalErrorException("Internal Error: either ba_id or user_id shouldnt be null");
        } 
        User user = mongoHelperFindUser(user_id);
        if (user == null) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        boolean removed = false;
        if (user.removeBankAccount(ba_id)) {
            mongoHelperUpdateUserAccounts(user_id, user.myBankAccounts());
            removed = true;
        }
        if (!removed) {
            throw new ResourceNotFoundException(ba_id,
                                                noUserBAMesg(user_id, ba_id));
        }
    }
    
}
