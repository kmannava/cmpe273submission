package hello.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalErrorException extends RuntimeException {
    private final String message;
 
    public InternalErrorException(String message) { 
        this.message = message;
    }
    
    public String getMessage() {
        return this.message;
    }

};
