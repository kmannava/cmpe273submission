package hello.api.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.*;
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseStatus;

import hello.api.resources.CommonController;
import hello.domain.WebLogin;

@RestController("/api/v1/users/{user_id}/weblogins")
public class WebLoginController extends CommonController {

    
    //create WebLogin
    @RequestMapping(value = "/api/v1/users/{user_id}/weblogins", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public WebLogin createWebLogin(@PathVariable String user_id,
                                   @Valid @RequestBody WebLogin weblogin) {
        myrepo.addWebLogin(user_id,weblogin);
        return weblogin;
    }

    // get all WebLogins
    @RequestMapping(value = "/api/v1/users/{user_id}/weblogins", method = RequestMethod.GET)
    public ResponseEntity <List< WebLogin>> getWebLogin(@PathVariable String user_id) {
        List <WebLogin> ll =  myrepo.getWebLogins(user_id);
        return new ResponseEntity<List< WebLogin>>(ll, HttpStatus.OK);
    }

    // get particular weblogin 
    @RequestMapping(value = "/api/v1/users/{user_id}/weblogins/{login_id}", 
                    method = RequestMethod.GET)
    public ResponseEntity <WebLogin> getWebLogin(@PathVariable String user_id,
                                                 @PathVariable String login_id) {
        WebLogin w = myrepo.getWebLogin(user_id, login_id);
        return new ResponseEntity<WebLogin>(w, HttpStatus.OK);
    }

    // delete a particular weblogin
    @RequestMapping(value = "/api/v1/users/{user_id}/weblogins/{login_id}", 
                    method = RequestMethod.DELETE)
    public ResponseEntity <String> deleteWebLogin(@PathVariable String user_id,
                                                  @PathVariable String login_id) {
        myrepo.deleteWebLogin(user_id, login_id);
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }

}
