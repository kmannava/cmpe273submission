package hello.api.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.*;
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseStatus;

import hello.api.resources.CommonController;
import hello.domain.BankAccount;

@RestController("/api/v1/users/{user_id}/bankaccounts")
public class BankAccountController extends CommonController {

    
    //create BankAccount
    @RequestMapping(value = "/api/v1/users/{user_id}/bankaccounts", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public BankAccount createBankAccount(@PathVariable String user_id,
                                         @Valid @RequestBody BankAccount weblogin) {
        myrepo.addBankAccount(user_id,weblogin);
        return weblogin;
    }

    // get all BankAccounts
    @RequestMapping(value = "/api/v1/users/{user_id}/bankaccounts", method = RequestMethod.GET)
    public ResponseEntity <List< BankAccount>> getBankAccount(@PathVariable String user_id) {
        List <BankAccount> ll =  myrepo.getBankAccounts(user_id);
        return new ResponseEntity<List< BankAccount>>(ll, HttpStatus.OK);
    }

    // get particular weblogin 
    @RequestMapping(value = "/api/v1/users/{user_id}/bankaccounts/{ba_id}", 
                    method = RequestMethod.GET)
    public ResponseEntity <BankAccount> getBankAccount(@PathVariable String user_id,
                                                       @PathVariable String ba_id) {
        BankAccount w =  myrepo.getBankAccount(user_id, ba_id);
        return new ResponseEntity<BankAccount>(w, HttpStatus.OK);
    }

    // delete a particular weblogin
    @RequestMapping(value = "/api/v1/users/{user_id}/bankaccounts/{ba_id}", 
                    method = RequestMethod.DELETE)
    public ResponseEntity <String> deleteBankAccount(@PathVariable String user_id,
                                                     @PathVariable String ba_id) {
        myrepo.deleteBankAccount(user_id, ba_id);
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }

}
