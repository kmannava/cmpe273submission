package hello.domain;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


public class WebLogin {

    // system generated
    private String login_id;
  
    // required
    @NotNull
    private String url;
    @NotNull
    private String login;
    @NotNull
    private String password;
    
    public WebLogin(){
        this.login_id = ""; 
        this.url = "";
        this.password = "";
        this.login = ""; 
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return this.login;
    }
 
    public void setPassword(String w_password) {
        this.password = w_password;
    } 
    public String getPassword() {
        return this.password;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }

    public String getLogin_id() {
        return this.login_id;
    }
    
}   
