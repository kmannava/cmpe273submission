package hello.api.resources;

import hello.domain.Greeting;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import com.justinsb.*;
import com.justinsb.etcd.EtcdClient;
import com.justinsb.etcd.EtcdResult;
import java.util.*;
import java.lang.*;

import java.net.URI;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public List<Greeting> greeting(@RequestParam(value="name", required=false, defaultValue="World") String name) {
        List <Greeting> ll = new ArrayList <Greeting>();
        ll.add(new Greeting(counter.incrementAndGet(),
                            String.format(template, "Shwetha")));
        ll.add(new Greeting(counter.incrementAndGet(),
                            String.format(template, "Ravi")));
 
        /*return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
        */
        return ll;
    }

    @RequestMapping("/api/v1/counter")
    public int incrementCounter() throws Exception{
        String mykey = "009325849/counter";
        String etcHost = "http://172.31.12.113:4001/";

        EtcdClient client = new EtcdClient(URI.create(etcHost));
	EtcdResult result = client.get(mykey);
	Integer foo = Integer.parseInt(result.node.value);
        foo = foo.intValue()+1;
	client.set(mykey, foo.toString());
	return foo.intValue();
    }
}
