package hello.api.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;
import org.springframework.web.bind.annotation.ResponseBody;  
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;

import hello.api.resources.CommonController;
import hello.exceptions.ResourceNotFoundException;
import hello.repository.UserRepository;
import hello.domain.User;
import hello.domain.WebLogin;


@RestController("/api/v1/users")
public class UserController extends CommonController {

    @RequestMapping(value = "/api/v1/users/{user_id}", method = RequestMethod.GET)
    public ResponseEntity <User> getUser(@PathVariable String user_id) {
        User user = myrepo.getUser(user_id);
        if (user != null) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } 
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/api/v1/users", method = RequestMethod.POST) 
    @ResponseStatus(HttpStatus.CREATED) 
    public User createUser(@Valid @RequestBody User user) {
        myrepo.addUser(user);
        return user;
    }
    
    @RequestMapping(value = "/api/v1/users/{user_id}", method = RequestMethod.PUT)
    public ResponseEntity <User> getUser(@PathVariable String user_id, 
                                         @Valid @RequestBody User reqUser) {
        User user = myrepo.getUser(user_id);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        User user2 = myrepo.modifyUser(user_id, reqUser);
        if (user2 == null) {
            new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<User>(user2, HttpStatus.OK);
    }

}
