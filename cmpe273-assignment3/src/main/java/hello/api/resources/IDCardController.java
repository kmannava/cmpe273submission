package hello.api.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.*;
import org.springframework.web.bind.annotation.RequestBody;  
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseStatus;

import hello.api.resources.CommonController;
import hello.domain.IDCard;

@RestController("/api/v1/users/{user_id}/idcards")
public class IDCardController extends CommonController {

    
    //create IDCard
    @RequestMapping(value = "/api/v1/users/{user_id}/idcards", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public IDCard createIDCard(@PathVariable String user_id,
                               @Valid @RequestBody IDCard idcard) {
        myrepo.addIDCard(user_id, idcard);
        return idcard;
    }

    // get all IDCards
    @RequestMapping(value = "/api/v1/users/{user_id}/idcards", method = RequestMethod.GET)
    public ResponseEntity <List< IDCard>> getIDCard(@PathVariable String user_id) {
        List <IDCard> ll =  myrepo.getIDCards(user_id);
        return new ResponseEntity<List< IDCard>>(ll, HttpStatus.OK);
    }

    // get particular idcard 
    @RequestMapping(value = "/api/v1/users/{user_id}/idcards/{card_id}", 
                    method = RequestMethod.GET)
    public ResponseEntity <IDCard> getIDCard(@PathVariable String user_id,
                                             @PathVariable String card_id) {
        IDCard w =  myrepo.getIDCard(user_id, card_id);
        return new ResponseEntity<IDCard>(w, HttpStatus.OK);
    }

    // delete a particular idcard
    @RequestMapping(value = "/api/v1/users/{user_id}/idcards/{card_id}", 
                    method = RequestMethod.DELETE)
    public ResponseEntity <String> deleteIDCard(@PathVariable String user_id,
                                                @PathVariable String card_id) {
        myrepo.deleteIDCard(user_id, card_id);
        return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
    }

}
