package hello.repository;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import hello.domain.User;
import hello.domain.WebLogin; 
import hello.domain.IDCard; 
import hello.domain.BankAccount; 
import hello.exceptions.ResourceNotFoundException;
import hello.exceptions.InternalErrorException;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.lang.System.*;

@Component
public class UserRepository {
    private final AtomicLong counter = new AtomicLong();

    public static ConcurrentHashMap<String, User> usersMap = new ConcurrentHashMap<String, User>();

    public static ConcurrentHashMap<String,ArrayList< WebLogin>> WebLoginMap = new 
        ConcurrentHashMap<String, ArrayList<WebLogin>>();

    public static ConcurrentHashMap<String,ArrayList< IDCard>> IDCardMap = new 
        ConcurrentHashMap<String, ArrayList<IDCard>>();

    public static ConcurrentHashMap<String,ArrayList< BankAccount>> BankAccountMap = new 
        ConcurrentHashMap<String, ArrayList<BankAccount>>();
    

//operations on User
    public User addUser(User newUser) {
        if (newUser == null) {
            return null;
        }
        String newUserId = getNewUserId();
        newUser.setUser_id(newUserId);
        newUser.setCreated_at(getCurrentTimestamp());
        newUser.setUpdated_at(getCurrentTimestamp());
        usersMap.putIfAbsent(newUserId, newUser);

        WebLoginMap.putIfAbsent(newUserId, new ArrayList<WebLogin>());
        IDCardMap.putIfAbsent(newUserId, new ArrayList<IDCard>());
        BankAccountMap.putIfAbsent(newUserId, new ArrayList<BankAccount>());

        return newUser;
    }

    public User getUser(String user_id) {
        if (user_id == null) {
            return null;
        }
        return usersMap.get(user_id);
    }
 
    public User modifyUser(String user_id, final User newValue) {
        User oldUser = usersMap.get(user_id);
        /* TODO if (oldUser.getEmail() != newValue.getEmail()) {
            //return invalid input
            return null;
        }*/
        oldUser.setPassword(newValue.password);
        oldUser.setName(newValue.name);
        oldUser.setUpdated_at(getCurrentTimestamp());
        return oldUser;
    }
    
//operations for Weblogin

    public WebLogin addWebLogin(String user_id,WebLogin newWebLogin){
        if(usersMap.containsKey(user_id) ){
            setproperties(newWebLogin);
            if(WebLoginMap.get(user_id) == null){
                WebLoginMap.put(user_id, new ArrayList<WebLogin>());
            }
            ArrayList<WebLogin> l = WebLoginMap.get(user_id);
            l.add(newWebLogin);
            return newWebLogin;
        }else{
            return null;
        }
    }

    public boolean hasUser(String user_id) {
        return usersMap.containsKey(user_id);
    }

    public boolean hasWebLogin(String user_id, 
                               String login_id) {
        if (!hasUser(user_id) ) {
            return false;
        }
        ArrayList <WebLogin> ll = WebLoginMap.get(user_id);
        for (WebLogin wl: ll) {
            if (wl.getLogin_id().equals(login_id)) {
                return true;
            }
        }
        return false;
    }

    public WebLogin getWebLogin(String user_id, 
                                String login_id) {
        if (!hasUser(user_id) ) {
            return null;
        }
        ArrayList <WebLogin> ll = WebLoginMap.get(user_id);
        for (WebLogin wl: ll) {
            if (wl.getLogin_id().equals(login_id)) {
                return wl;
            }
        }
        return null;
    }

    public void deleteWebLogin(String user_id, 
                                String login_id) {
        if (!hasUser(user_id) ) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
        ArrayList <WebLogin> ll = WebLoginMap.get(user_id);
        for (WebLogin wl: ll) {
            if (wl.getLogin_id().equals(login_id)) {
                ll.remove(wl);
                return;
            }
        }
        String mesg = "Web login with id " + login_id + " not found";
        throw new ResourceNotFoundException(login_id, mesg);
    }

   public void setproperties(WebLogin newWebLogin){
        String login_id = "l-"+counter.incrementAndGet();
        newWebLogin.setLogin_id(login_id);
   }


    public ArrayList<WebLogin> getWebLogins(String user_id){
        ArrayList<WebLogin> ll = WebLoginMap.get(user_id);
        return ll;
    }

    private Date getCurrentTimestamp() {
        Date date= new Date();
        date.getTime();
        return date;
        //Timestamp ss = new Timestamp(date.getTime());
        //return ss.toString();
    }

    private String getNewUserId() {
        return "u-"+counter.incrementAndGet();
    }

    private void checkUserIDPresent(String user_id) {
        if (user_id == null ||
            !usersMap.containsKey(user_id)) {
            String mesg = "User with id " + user_id + " not found";
            throw new ResourceNotFoundException(user_id, mesg);
        }
    }

    // Operations for IDCard
    public void setCardID(IDCard idcard){
        String card_id = "c-"+counter.incrementAndGet();
        idcard.setCard_id(card_id);
    }

    private String noUserCardMesg(String user_id, 
                                  String card_id) {
        String mesg = String.format("User %s has no card with id %s", 
                                    user_id, 
                                    card_id);
        return mesg;
    }

    public IDCard addIDCard(String user_id,
                            IDCard newIDCard){
        if (newIDCard == null) {
            throw new InternalErrorException("Internal Error: user_id shouldnt be null");
        }
        checkUserIDPresent(user_id); 
        setCardID(newIDCard);
        if(IDCardMap.get(user_id) == null){
            IDCardMap.put(user_id, new ArrayList<IDCard>());
        }
        ArrayList<IDCard> ll = IDCardMap.get(user_id);
        ll.add(newIDCard);
        return newIDCard;
    }

    public ArrayList<IDCard> getIDCards(String user_id){
        checkUserIDPresent(user_id); 
        return IDCardMap.get(user_id);
    }

    public IDCard getIDCard(String user_id, 
                            String card_id) {
        if (user_id == null ||
            card_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or user_id shouldnt be null");
                                             
        }
        checkUserIDPresent(user_id); 
        ArrayList <IDCard> ll = IDCardMap.get(user_id);
        if (ll != null) {
            for (IDCard card: ll) {
                if (card.getCard_id().equals(card_id)) {
                    return card;
                }
            }
        }
        throw new ResourceNotFoundException(card_id, 
                                            noUserCardMesg(user_id, card_id));
    }

    public void deleteIDCard(String user_id, 
                             String card_id) {
        if (user_id == null ||
            card_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or user_id shouldnt be null");
        } 
        checkUserIDPresent(user_id);
        ArrayList <IDCard> ll = IDCardMap.get(user_id);
        if (ll != null) {
            for (IDCard card: ll) {
                if (card.getCard_id().equals(card_id)) {
                    ll.remove(card);
                    return;
                }
            }
        }
        throw new ResourceNotFoundException(card_id, 
                                            noUserCardMesg(user_id, card_id));
    }

    // Operations for BankAccount
    public void setBaID(BankAccount ba){
        String ba_id = "b-"+counter.incrementAndGet();
        ba.setBa_id(ba_id);
    }

    private String noUserBAMesg(String user_id, 
                                  String ba_id) {
        String mesg = String.format("User %s has no bank account with id %s", 
                                    user_id, 
                                    ba_id);
        return mesg;
    }

    public BankAccount addBankAccount(String user_id,
                                      BankAccount newBankAccount){
        if (user_id == null) {
            throw new InternalErrorException("Internal Error: user_id shouldnt be null");
        }
        checkUserIDPresent(user_id); 
        setBaID(newBankAccount);
        if(BankAccountMap.get(user_id) == null){
            BankAccountMap.put(user_id, new ArrayList<BankAccount>());
        }
        ArrayList<BankAccount> ll = BankAccountMap.get(user_id);
        ll.add(newBankAccount);
        return newBankAccount;
    }

    public ArrayList<BankAccount> getBankAccounts(String user_id){
        checkUserIDPresent(user_id); 
        return BankAccountMap.get(user_id);
    }

    public BankAccount getBankAccount(String user_id, 
                            String ba_id) {
        if (user_id == null ||
            ba_id == null) {
            throw new InternalErrorException("Internal Error: either card_id or ba_id shouldnt be null");
        }
        checkUserIDPresent(user_id); 
        ArrayList <BankAccount> ll = BankAccountMap.get(user_id);
        if (ll != null) {
            for (BankAccount card: ll) {
                if (card.getBa_id().equals(ba_id)) {
                    return card;
                }
            }
        }
        throw new ResourceNotFoundException(ba_id, 
                                            noUserBAMesg(user_id, ba_id));
    }

    public void deleteBankAccount(String user_id, 
                                  String ba_id) {
        if (user_id == null ||
            ba_id == null) {
            throw new InternalErrorException("Internal Error: either ba_id or user_id shouldnt be null");
        } 
        checkUserIDPresent(user_id);
        ArrayList <BankAccount> ll = BankAccountMap.get(user_id);
        if (ll != null) {
            for (BankAccount card: ll) {
                if (card.getBa_id().equals(ba_id)) {
                    ll.remove(card);
                    return;
                }
            }
        }
        throw new ResourceNotFoundException(ba_id,
                                            noUserBAMesg(user_id, ba_id));
    }
    
}
