#!/bin/bash

USER_ID="u-1"
SLEEP_TIME=1
# create user
echo "----------POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users -X POST -H "Content-Type: application/json" -d \
 '{ "name" : "shwetha", "email": "ra123vixx@gmail.com", "password": "12314" }'
echo -e "\n"

#get user
sleep $SLEEP_TIME 
echo "---------GET -----------------------------------"
curl -i 54.193.114.91:8080/api/v1/users/$USER_ID
echo -e "\n"

#modify user
sleep $SLEEP_TIME 
echo "---------PUT -----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID -X PUT -H "Content-Type: application/json" -d \
 '{ "name" : "mannava", "email": "ra123vixx@gmail.com", "password": "23423" }'
echo -e "\n"


#weblogin
sleep $SLEEP_TIME

echo "----------WEB LOGIN POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/weblogins -X POST -H "Content-Type: application/json" -d \
 '{ "url" : "mannava", "login": "1234", "password": "23423" }'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------WEB LOGIN POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/weblogins -X POST -H "Content-Type: application/json" -d \
 '{ "url" : "sjsu.com", "login": "1234", "password": "23423" }'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------WEB LOGIN GET LIST-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/weblogins
echo -e "\n"
sleep $SLEEP_TIME

echo "----------WEB LOGIN GET-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/weblogins/l-2
echo -e "\n"
sleep $SLEEP_TIME

echo "----------WEB LOGIN DELETE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/weblogins/l-2 -X DELETE
echo -e "\n"
sleep $SLEEP_TIME


#ID
#read -p "Do you wish start CARD APIS yn?" yn
echo "----------ID POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/idcards -X POST -H "Content-Type: application/json" -d \
 '{ "card_name": "San Jose Public Library Card",  "card_number": "11213323",  "expiration_date": "11-06-2014" }'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------ID POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/idcards -X POST -H "Content-Type: application/json" -d \
 '{ "card_name": "San Jose Public Library Card",  "card_number": "11323",  "expiration_date": "12-31-2014" }'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------ID GET LIST-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/idcards
echo -e "\n"
sleep $SLEEP_TIME

echo "----------ID GET-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/idcards/c-4
echo -e "\n"
sleep $SLEEP_TIME

echo "----------ID LOGIN DELETE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/idcards/c-4 -X DELETE
echo -e "\n"
sleep $SLEEP_TIME


#BA
#read -p "Do you wish start BANK ACCOUNT APIS yn?" yn
echo "----------BA POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/bankaccounts -X POST -H "Content-Type: application/json" -d \
'{"account_name": "My Bank Of America Checking", "routing_number": "121000358","account_number": "040834236"}'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------BA POST CREATE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/bankaccounts -X POST -H "Content-Type: application/json" -d \
'{"account_name": "My Bank Of America Checking", "routing_number": "131000358","account_number": "140834236"}'
echo -e "\n"
sleep $SLEEP_TIME

echo "----------BA GET LIST-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/bankaccounts
echo -e "\n"
sleep $SLEEP_TIME

echo "----------BA GET-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/bankaccounts/b-6
echo -e "\n"
sleep $SLEEP_TIME

echo "----------BA DELETE-----------------------------------"
curl -i http://54.193.114.91:8080/api/v1/users/$USER_ID/bankaccounts/b-6 -X DELETE
echo -e "\n"
sleep $SLEEP_TIME


