package hello.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private String resourceId;
    private final String message;
 
    public ResourceNotFoundException(String id, 
                                     String message) {
        this.resourceId = id;
        this.message = message;
    }
    
    public void setResourceId(String id) {
        this.resourceId = id;
    }

    public String getResourceId() {
        return this.resourceId;
    }
    public String getMessage() {
        return this.message;
    }

};
