package hello.api.resources;

import hello.domain.Greeting;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public List<Greeting> greeting(@RequestParam(value="name", required=false, defaultValue="World") String name) {
        List <Greeting> ll = new ArrayList <Greeting>();
        ll.add(new Greeting(counter.incrementAndGet(),
                            String.format(template, "Shwetha")));
        ll.add(new Greeting(counter.incrementAndGet(),
                            String.format(template, "Ravi")));
 
        /*return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
        */
        return ll;
    }
}
