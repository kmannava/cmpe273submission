package hello.api.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;  
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.HttpStatus;

import hello.exceptions.ResourceNotFoundException;
import hello.repository.UserRepository;


public class CommonController {

    @Autowired
    public UserRepository myrepo;

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleException(ResourceNotFoundException e) {
        return e.getMessage();
    }
}
