package hello.domain;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.text.SimpleDateFormat;

public class User {

    // system generated
    public String user_id;
    private Date updated_at;
    private Date created_at;
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    // required
    @NotNull
    //TODO @Pattern(regexp="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$")
    public String email;
    @NotNull
    public String password;

    //optional
    public String name;
    
    public User(){
        this.user_id = "";
        this.email = "";
        this.password = "";
        this.name = "";
        this.updated_at = null;
        this.created_at = null; 

    
    }
    
    public void setCreated_at(Date time) {
        this.created_at = time;
    }

    public String getCreated_at() {
        return sdf.format(this.created_at);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    
    public void setPassword(String pwd) {
        this.password = pwd;
    }

    public String getPassword() {
        return this.password;
    }
 
    public void setEmail(String email) {
        this.email = email;
    } 
    public String getEmail() {
        return this.email;
    }


    public void setUpdated_at(Date time) {
        this.updated_at = time;
    }

    public String getUpdated_at() {
        return sdf.format(this.updated_at);
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }
    
}   
