package hello.domain;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


public class BankAccount {

    // system generated
    private String ba_id;
  
    // required
    @NotNull
    private String routing_number;
    @NotNull
    private String account_number;
    @NotNull
    private String account_name;
    
    public BankAccount(){
        this.ba_id = ""; 
        this.routing_number = ""; 
        this.account_number = "";
        this.account_name = "";
    }
    
    public void setBa_id(String ba_id) {
        this.ba_id = ba_id;
    }

    public String getBa_id() {
        return this.ba_id;
    }
    
    public void setRouting_number(String routing_number) {
        this.routing_number = routing_number;
    }

    public String getRouting_number() {
        return this.routing_number;
    }
    
    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getAccount_number() {
        return this.account_number;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_name() {
        return this.account_name;
    }
}   
